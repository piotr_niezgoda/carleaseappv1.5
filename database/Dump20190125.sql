CREATE DATABASE  IF NOT EXISTS `CarLeaseApp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `CarLeaseApp`;
-- MySQL dump 10.13  Distrib 8.0.14, for Linux (x86_64)
--
-- Host: localhost    Database: CarLeaseApp
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Tcars`
--

DROP TABLE IF EXISTS `Tcars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Tcars` (
  `carID` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `basePrice` decimal(10,2) NOT NULL,
  `subType` varchar(20) NOT NULL,
  PRIMARY KEY (`carID`),
  KEY `fk_Tcars_1_idx` (`subType`),
  CONSTRAINT `fk_Tcars_1` FOREIGN KEY (`subType`) REFERENCES `TsubTypes` (`subtype`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tcars`
--

LOCK TABLES `Tcars` WRITE;
/*!40000 ALTER TABLE `Tcars` DISABLE KEYS */;
INSERT INTO `Tcars` VALUES (1,'Skoda','Octavia',100.00,'kombi'),(2,'Skoda','Octavia',90.00,'sedan'),(3,'Skoda','Fabia',65.00,'hatchback'),(4,'Opel','Astra',70.00,'hatchback'),(5,'Opel','Astra',75.00,'kombi'),(6,'Opel','Insignia',95.00,'hatchback'),(7,'Opel','Insignia',105.00,'kombi'),(8,'Porsche','911',1000.00,'kabriolet'),(10,'BMW','X5',900.00,'suv'),(11,'BMW','X3',750.00,'suv'),(12,'Volvo','XC40',500.00,'suv'),(13,'Volvo','XC60',600.00,'suv'),(14,'Volvo','V60',400.00,'kombi'),(15,'Volvo','V60',385.00,'sedan'),(16,'Fiat','500',70.00,'hatchback'),(17,'Volvo','FH16',600.00,'ciągnik'),(18,'Mercedes','Actros',650.00,'ciągnik'),(19,'DAF','XF',649.00,'ciągnik'),(20,'Mercedes','Sprinter',549.00,'bus'),(21,'Wielton','Coil Master',200.00,'naczepa'),(22,'Wielton','Strong Master',300.00,'naczepa'),(23,'Wielton','Weight Master',275.00,'naczepa'),(24,'CAT','MH3024',286.00,'koparka'),(25,'CAT','428F2',277.00,'koparka'),(26,'JCB','JS130',297.00,'koparka'),(27,'JCB','VM137D',281.00,'walec'),(28,'Audi','A4',315.48,'sedan'),(29,'Audi','A4',315.57,'hatchback'),(33,'Bomag','BW 174 AD',98.00,'walec');
/*!40000 ALTER TABLE `Tcars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TmysteriousVariables`
--

DROP TABLE IF EXISTS `TmysteriousVariables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `TmysteriousVariables` (
  `mysteriousVariableID` int(11) NOT NULL AUTO_INCREMENT,
  `mysteriousVariableValue` int(11) NOT NULL,
  PRIMARY KEY (`mysteriousVariableID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TmysteriousVariables`
--

LOCK TABLES `TmysteriousVariables` WRITE;
/*!40000 ALTER TABLE `TmysteriousVariables` DISABLE KEYS */;
INSERT INTO `TmysteriousVariables` VALUES (1,13);
/*!40000 ALTER TABLE `TmysteriousVariables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Treservations`
--

DROP TABLE IF EXISTS `Treservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Treservations` (
  `reservationID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `carID` int(11) NOT NULL,
  `date` date NOT NULL,
  `reservationCost` decimal(10,2) NOT NULL,
  PRIMARY KEY (`reservationID`),
  KEY `fk_Treservations_1_idx` (`userID`),
  KEY `fk_Treservations_2_idx` (`carID`),
  CONSTRAINT `fk_Treservations_1` FOREIGN KEY (`userID`) REFERENCES `Tusers` (`userid`),
  CONSTRAINT `fk_Treservations_2` FOREIGN KEY (`carID`) REFERENCES `Tcars` (`carid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Treservations`
--

LOCK TABLES `Treservations` WRITE;
/*!40000 ALTER TABLE `Treservations` DISABLE KEYS */;
INSERT INTO `Treservations` VALUES (8,3,1,'2019-06-22',1500.00);
/*!40000 ALTER TABLE `Treservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TsubTypes`
--

DROP TABLE IF EXISTS `TsubTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `TsubTypes` (
  `subType` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`subType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TsubTypes`
--

LOCK TABLES `TsubTypes` WRITE;
/*!40000 ALTER TABLE `TsubTypes` DISABLE KEYS */;
INSERT INTO `TsubTypes` VALUES ('bus','ciężarowy'),('ciągnik','ciężarowy'),('hatchback','osobowy'),('kabriolet','osobowy'),('kombi','osobowy'),('koparka','maszyna'),('naczepa','ciężarowy'),('sedan','osobowy'),('suv','osobowy'),('walec','maszyna');
/*!40000 ALTER TABLE `TsubTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tusers`
--

DROP TABLE IF EXISTS `Tusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Tusers` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  `userType` varchar(10) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tusers`
--

LOCK TABLES `Tusers` WRITE;
/*!40000 ALTER TABLE `Tusers` DISABLE KEYS */;
INSERT INTO `Tusers` VALUES (1,'admin','ytrewq','admin','',''),(2,'jkowalsky','qwerty','admin','John','Kowalsky'),(3,'smith72','','user','Andrew','Smith'),(4,'qbdan92','','user','',''),(5,'bogdanbogdan','','user','Bogdan',''),(6,'marylee','','user','Mary','Lee'),(12,'asshole',NULL,'user',NULL,NULL),(13,'asshole2',NULL,'user',NULL,NULL),(14,'asshole3','123','user','ass','hole'),(15,'asshole666','qwe','user',NULL,NULL),(16,'asshole777',NULL,'user',NULL,NULL),(17,'asshole666777','666777','user','asshole666777','ass');
/*!40000 ALTER TABLE `Tusers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-25 23:33:12
