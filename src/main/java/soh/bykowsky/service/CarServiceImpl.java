package soh.bykowsky.service;

import org.springframework.stereotype.Component;
import soh.bykowsky.DBhandler;
import soh.bykowsky.model.Car;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CarServiceImpl implements CarService {
    public Car getCar(int carID) {
        try {
            String SQL = "SELECT CarLeaseApp.Tcars.brand, CarLeaseApp.Tcars.name, CarLeaseApp.Tcars.basePrice, CarLeaseApp.Tcars.subType, CarLeaseApp.TsubTypes.type FROM CarLeaseApp.Tcars JOIN CarLeaseApp.TsubTypes ON CarLeaseApp.Tcars.subType = CarLeaseApp.TsubTypes.subType WHERE carID = ?";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, carID);
            ResultSet results = preparedStatement.executeQuery();

            Car carFromDB = new Car();
            while (results.next()) {
                carFromDB.setCarId(carID);
                carFromDB.setBrand(results.getString("brand"));
                carFromDB.setName(results.getString("name"));
                carFromDB.setBasePrice(results.getDouble("basePrice"));
                carFromDB.setSubType(results.getString("subType"));
                carFromDB.setType(results.getString("type"));
            }

            return carFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Car getCar(String brand, String name) {
        try {
            String SQL = "SELECT CarLeaseApp.Tcars.carID, CarLeaseApp.Tcars.basePrice, CarLeaseApp.Tcars.subType, CarLeaseApp.TsubTypes.type FROM CarLeaseApp.Tcars JOIN CarLeaseApp.TsubTypes ON CarLeaseApp.Tcars.subType = CarLeaseApp.TsubTypes.subType WHERE brand = ? AND name = ? LIMIT 1";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, brand);
            preparedStatement.setString(2, name);
            ResultSet results = preparedStatement.executeQuery();

            Car carFromDB = new Car();
            while (results.next()) {
                carFromDB.setCarId(results.getInt("carID"));
                carFromDB.setBrand(brand);
                carFromDB.setName(name);
                carFromDB.setBasePrice(results.getDouble("basePrice"));
                carFromDB.setSubType(results.getString("subType"));
                carFromDB.setType(results.getString("type"));
            }

            return carFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Car> getCarsAll() {
        try {
            String SQL = "SELECT CarLeaseApp.Tcars.carID, CarLeaseApp.Tcars.brand, CarLeaseApp.Tcars.name, CarLeaseApp.Tcars.basePrice, CarLeaseApp.Tcars.subType, CarLeaseApp.TsubTypes.type FROM CarLeaseApp.Tcars JOIN CarLeaseApp.TsubTypes ON CarLeaseApp.Tcars.subType = CarLeaseApp.TsubTypes.subType ORDER BY carID";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            ResultSet results = preparedStatement.executeQuery();

            List<Car> carsFromDB = new ArrayList<Car>();
            while (results.next()) {
                Car carFromDB = new Car();
                carFromDB.setCarId(results.getInt("carID"));
                carFromDB.setBrand(results.getString("brand"));
                carFromDB.setName(results.getString("name"));
                carFromDB.setBasePrice(results.getDouble("basePrice"));
                carFromDB.setSubType(results.getString("subType"));
                carFromDB.setType(results.getString("type"));

                carsFromDB.add(carFromDB);
            }

            return carsFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Car> getCarsByBrand(String brand) {
        try {
            String SQL = "SELECT CarLeaseApp.Tcars.carID, CarLeaseApp.Tcars.name, CarLeaseApp.Tcars.basePrice, CarLeaseApp.Tcars.subType, CarLeaseApp.TsubTypes.type FROM CarLeaseApp.Tcars JOIN CarLeaseApp.TsubTypes ON CarLeaseApp.Tcars.subType = CarLeaseApp.TsubTypes.subType WHERE brand = ?";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, brand);
            ResultSet results = preparedStatement.executeQuery();

            List<Car> carsFromDB = new ArrayList<Car>();
            while (results.next()) {
                Car carFromDB = new Car();
                carFromDB.setCarId(results.getInt("carID"));
                carFromDB.setBrand(brand);
                carFromDB.setName(results.getString("name"));
                carFromDB.setBasePrice(results.getDouble("basePrice"));
                carFromDB.setSubType(results.getString("subType"));
                carFromDB.setType(results.getString("type"));

                carsFromDB.add(carFromDB);
            }

            return carsFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Car> getCarsByType(String type) {
        try {
            String SQL = "SELECT CarLeaseApp.Tcars.carID, CarLeaseApp.Tcars.brand, CarLeaseApp.Tcars.name, CarLeaseApp.Tcars.basePrice, CarLeaseApp.Tcars.subType FROM CarLeaseApp.Tcars JOIN CarLeaseApp.TsubTypes ON CarLeaseApp.Tcars.subType = CarLeaseApp.TsubTypes.subType WHERE type = ? ORDER BY carID";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, type);
            ResultSet results = preparedStatement.executeQuery();

            List<Car> carsFromDB = new ArrayList<Car>();
            while (results.next()) {
                Car carFromDB = new Car();
                carFromDB.setCarId(results.getInt("carID"));
                carFromDB.setBrand(results.getString("brand"));
                carFromDB.setName(results.getString("name"));
                carFromDB.setBasePrice(results.getDouble("basePrice"));
                carFromDB.setSubType(results.getString("subType"));
                carFromDB.setType(type);

                carsFromDB.add(carFromDB);
            }

            return carsFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Car> getCarsBySubType(String subType) {
        try {
            String SQL = "SELECT CarLeaseApp.Tcars.carID, CarLeaseApp.Tcars.brand, CarLeaseApp.Tcars.name, CarLeaseApp.Tcars.basePrice, CarLeaseApp.TsubTypes.type FROM CarLeaseApp.Tcars JOIN CarLeaseApp.TsubTypes ON CarLeaseApp.Tcars.subType = CarLeaseApp.TsubTypes.subType WHERE CarLeaseApp.Tcars.subType = ? ORDER BY carID";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, subType);
            ResultSet results = preparedStatement.executeQuery();

            List<Car> carsFromDB = new ArrayList<Car>();
            while (results.next()) {
                Car carFromDB = new Car();
                carFromDB.setCarId(results.getInt("carID"));
                carFromDB.setBrand(results.getString("brand"));
                carFromDB.setName(results.getString("name"));
                carFromDB.setBasePrice(results.getDouble("basePrice"));
                carFromDB.setSubType(subType);
                carFromDB.setType(results.getString("type"));

                carsFromDB.add(carFromDB);
            }

            return carsFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean addCar(Car car) {
        try {
            String SQL = "INSERT INTO Tcars (brand, name, basePrice, subType) VALUES (?, ?, ?, ?)";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setString(2, car.getName());
            preparedStatement.setDouble(3, car.getBasePrice());
            preparedStatement.setString(4, car.getSubType());

            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean deleteCar(int carID) {
        try {
            String SQL = "DELETE FROM Tcars WHERE carID = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, carID);
            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateCar(int id, Car car){
        try {
            String SQL = "UPDATE Tcars SET brand = ?, name = ?, basePrice = ?, subType = ? WHERE carID = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setString(2, car.getName());
            preparedStatement.setDouble(3,car.getBasePrice());
            preparedStatement.setString(4,car.getSubType());
            preparedStatement.setInt(5,id);
            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    //TODO Method not tested yet
    public List<String> getBrands(){
        try {
            String SQL = "SELECT DISTINCT CarLeaseApp.Tcars.brand FROM CarLeaseApp.Tcars";

            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            ResultSet results = preparedStatement.executeQuery();

            List<String> brandsFromDB = new ArrayList<String>();
            while (results.next()) {

                brandsFromDB.add(results.getString("brand"));
            }

            return brandsFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
