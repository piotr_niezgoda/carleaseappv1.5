package soh.bykowsky.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import soh.bykowsky.model.Car;
import soh.bykowsky.model.Reservation;
import soh.bykowsky.model.User;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

@Component
public class UserInterface {
    public static final String NO_SUCH_OPTION = "No such option available. Please try again.";      //TODO Duplicated in MainAppLoop class also. Need to think it through
    Scanner scanner = new Scanner(System.in);                                                       //TODO Duplicated in MainAppLoop class also. Need to think it through

    @Autowired
    CarService carService;

    @Autowired
    UserService userService;

    @Autowired
    ReservationService reservationService;

    @Autowired
    LoginVerificator loginVerificator;

    @Autowired
    MysteriousVariableServiceImpl mysteriousVariableService;

    @Autowired
    PriceCalculator priceCalculator;

    public void printAppTitle() {
        System.out.println("********************************");
        System.out.println("* Welcome to the CAR LEASE APP *");
        System.out.println("********************************");
        System.out.println("");
    }

    public void printMainMenu() {
        printAppTitle();

        System.out.println("/--------- MAIN MENU ---------/");
        System.out.println("1. Login as administrator");
        System.out.println("2. Login as registered user");
        System.out.println("3. Login as guest");
        System.out.println("4. Register as new user");
        System.out.println("0. Quit the application");
        System.out.println("");
        System.out.println("");
    }

    public void printAdminMenu() {
        System.out.println("/--------- ADMIN MENU ---------/");
        System.out.println("1. List all cars");
        System.out.println("2. List all civilian cars");
        System.out.println("3. List all trucks");
        System.out.println("4. List all construction vehicles");
        System.out.println("5. Add car");
        System.out.println("6. Duplicate car");
        System.out.println("7. Update car");
        System.out.println("8. Remove car");
        System.out.println("0. Go back to MAIN MENU");
        System.out.println("");
        System.out.println("");
    }

    public void printUserMenu() {
        System.out.println("/--------- USER MENU ---------/");
        System.out.println("1. List all our cars");
        System.out.println("2. List civilian cars");
        System.out.println("3. List trucks");
        System.out.println("4. List construction vehicles");
        System.out.println("5. Book a car");
        System.out.println("6. List your bookings");
        System.out.println("0. Go back to MAIN MENU");
        System.out.println("");
        System.out.println("");
    }

    public void printGuestMenu() {
        System.out.println("/--------- GUEST MENU ---------/");
        System.out.println("1. List all our cars");
        System.out.println("2. List civilian cars");
        System.out.println("3. List trucks");
        System.out.println("4. List construction vehicles");
        System.out.println("5. Register as new user");
        System.out.println("0. Go back to MAIN MENU");
        System.out.println("");
        System.out.println("");
    }

    public void printCars(Car car) {
        System.out.println(car.getBrand() + " " + car.getName() + " " + car.getSubType() + " | base price: " + car.getBasePrice());
    }

    public void printCars(List<Car> cars) {
        for (Car car : cars) {
            System.out.println(car.getBrand() + " " + car.getName() + " " + car.getSubType() + " | base price: " + car.getBasePrice());
        }
    }

    public void printCarsWithID(Car car) {
        System.out.println("ID:" + car.getCarId() + "  " + car.getBrand() + " " + car.getName() + " " + car.getSubType() + " | base price: " + car.getBasePrice());
    }

    public void printCarsWithID(List<Car> cars) {
        for (Car car : cars) {
            System.out.println("ID:" + car.getCarId() + "  " + car.getBrand() + " " + car.getName() + " " + car.getSubType() + " | base price: " + car.getBasePrice());
        }
    }

    public void printReservation(Reservation reservation) {
        Car car = carService.getCar(reservation.getCarID());
        System.out.println("Car: " + car.getBrand() + " " + car.getName() + " reservation data: " + reservation.getDate() + " total reservation cost: " + reservation.getReservationCost());
    }

    public void printReservation(List<Reservation> reservations) {
        for (Reservation reservation : reservations) {
            Car car = carService.getCar(reservation.getCarID());
            System.out.println("Car: " + car.getBrand() + " " + car.getName() + " reservation data: " + reservation.getDate() + " total reservation cost: " + reservation.getReservationCost());
        }
    }

    public void printReservationWithID(Reservation reservation) {
        Car car = carService.getCar(reservation.getCarID());
        System.out.println("Reservation ID: " + reservation.getReservationID() + "Car: " + car.getBrand() + " " + car.getName() + " reservation data: " + reservation.getDate() + " total reservation cost: " + reservation.getReservationCost());
    }

    public void printReservationWithID(List<Reservation> reservations) {
        for (Reservation reservation : reservations) {
            Car car = carService.getCar(reservation.getCarID());
            System.out.println("Reservation ID: " + reservation.getReservationID() + "Car: " + car.getBrand() + " " + car.getName() + " reservation data: " + reservation.getDate() + " total reservation cost: " + reservation.getReservationCost());
        }
    }

    public Car addNewCar() {
        System.out.println("Adding car to the database...");
        Car newCar = new Car();

        try {

            boolean finish = true;
            do {    //TODO make While() loop instead???
                finish = true;  //FIXME Was missing of this line being a cause for bugs in switch statement down the end of this method???

                boolean correctPickForType = true;
                do {
                    System.out.println("1. Civilian, 2. Truck, 3. Construction Vehicle");
                    System.out.print("pick: ");

                    int carTypePick = scanner.nextInt();
                    scanner.nextLine();

                    switch (carTypePick) {
                        case 1:
                            newCar.setType("osobowy");
                            break;
                        case 2:
                            newCar.setType("ciężarowy");
                            break;
                        case 3:
                            newCar.setType("maszyna");
                            break;
                        default:
                            System.out.println(NO_SUCH_OPTION);
                            correctPickForType = false;
                            break;
                    }
                } while (!correctPickForType);

                System.out.print("Provide brand: ");
                String newBrand = scanner.nextLine();
                newCar.setBrand(newBrand);

                System.out.print("Provide name: ");
                String newName = scanner.nextLine();
                newCar.setName(newName);

                //TODO Need to hardcode sub-type, as per requirement (same as type, with switch statement?)
                System.out.print("Provide sub type: ");
                String newSubType = scanner.nextLine();
                newCar.setSubType(newSubType);

                System.out.print("Provide base price: ");
                double newBasePrice = scanner.nextDouble();
                scanner.nextLine();
                newCar.setBasePrice(newBasePrice);

                System.out.println("");
                System.out.println("Here is the new car");
                printCars(newCar);
                System.out.println("");
                System.out.println("Do you want to add it to data base or try again?");
                System.out.println("1. Add to database, 2. Try again, 0. Go back to ADMIN menu");       //TODO Need to implement 0. option
                System.out.print("Pick: ");

                int addOrTryAgain = scanner.nextInt();
                scanner.nextLine();

                switch (addOrTryAgain) {    //FIXME This may not work as intended
                    case 1:
                        break;
                    case 2:
                        finish = false;
                        break;
                    default:
                        break;
                }

            } while (!finish);


            System.out.println("Adding new car to database...");
            return newCar;
        } catch (InputMismatchException e) {
            e.printStackTrace();
            return newCar;
        }
    }

    public int deleteCar() {
        System.out.println("Removing car from the database...");

        int carID = 0;

        try {
            boolean finish;
            do {    //TODO make While() loop instead???
                finish = true;
                System.out.print("Provide car ID: ");
                carID = scanner.nextInt();                  // FIXME During one test run, input missmatch except happend. Need to figure out this bug.
                scanner.nextLine();

                System.out.print("Car to be removed from database: ");
                printCarsWithID(carService.getCar(carID));
                System.out.println("Are you sure to remove this car from the database?");
                System.out.println("1. Remove from the database, 2. Try again, 0. Go back to ADMIN menu");  //TODO Need to implement 0. option
                System.out.print("Pick: ");

                int userPick = scanner.nextInt();
                scanner.nextLine();

                switch (userPick) {
                    case 1:
                        break;
                    case 2:
                        finish = false;
                        break;
                    default:
                        finish = false;
                        break;
                }
            } while (!finish);

        } catch (InputMismatchException e) {
            e.printStackTrace();
        }

        return carID;
    }

    public void updateCar() {
        System.out.println("Updating car in the database...");
        Car newCar = new Car();
        int carID = 0;

        try {

            boolean finish = true;
            do {

                boolean correctPickForID = true;
                do {
                    System.out.print("Which car do you want to update? Provide car ID: ");
                    carID = scanner.nextInt();
                    scanner.nextLine();

                    System.out.print("Car to be updated: ");
                    printCars(carService.getCar(carID));
                    System.out.println("Are you sure to update this car?");
                    System.out.println("1. Update, 2. Provide car ID again, 0. Go back to ADMIN menu");  //TODO Need to implement 0. option
                    System.out.print("Pick: ");

                    int userPick = scanner.nextInt();
                    scanner.nextLine();

                    switch (userPick) {
                        case 1:
                            break;
                        case 2:
                            correctPickForID = false;
                            break;
                        default:
                            correctPickForID = false;
                            break;
                    }
                } while (!correctPickForID);

                System.out.print("Provide brand: ");
                String newBrand = scanner.nextLine();
                newCar.setBrand(newBrand);

                System.out.print("Provide name: ");
                String newName = scanner.nextLine();
                newCar.setName(newName);

                //TODO Need to hardcode sub-type, as per requirement (same as type, with switch statement?)
                System.out.print("Provide sub type: ");
                String newSubType = scanner.nextLine();
                newCar.setSubType(newSubType);

                System.out.print("Provide base price: ");
                double newBasePrice = scanner.nextDouble();
                scanner.nextLine();
                newCar.setBasePrice(newBasePrice);

                System.out.println("");
                System.out.println("Here is the car after update");
                printCars(newCar);
                System.out.println("");
                System.out.println("Do you want to save it into the database or try again?");
                System.out.println("1. Add to database, 2. Try again, 0. Go back to ADMIN menu");       //TODO Need to implement 0. option
                System.out.print("Pick: ");

                int addOrTryAgain = scanner.nextInt();
                scanner.nextLine();

                switch (addOrTryAgain) {    //FIXME This may not work as intended
                    case 1:
                        finish = true;
                        break;
                    case 2:
                        finish = false;
                        break;
                    default:
                        finish = false;
                        break;
                }

            } while (!finish);

            System.out.println("Adding new car to database...");
            carService.updateCar(carID, newCar);


        } catch (InputMismatchException e) {
            e.printStackTrace();
        }
    }

    public void duplicateCar() {
        System.out.println("Sorry. This feature is not implemented yet.");
    }

    public void registerUser() {
        //TODO Implement to check if login is already taken. Login is unique in Database
        try {

            System.out.println("New user registration...");
            System.out.print("Please provide login: ");
            String newLogin = scanner.nextLine();

            User newUser = new User(newLogin, "user");

            String newPassword = new String();
            String newPasswordDoubleCheck = new String();
            boolean passwordsMatch = false;
            do {
                System.out.print("Please provide password");
                newPassword = scanner.nextLine();


                System.out.print("Retype password");
                newPasswordDoubleCheck = scanner.nextLine();

                if (newPassword.equals(newPasswordDoubleCheck)) {
                    passwordsMatch = true;
                } else {
                    System.out.println("Retyped password does not match your new password");
                }
            } while (!passwordsMatch);

            if (!newPassword.equals("")) {
                newUser.setPassword(newPassword);
            }

            System.out.print("Please provide your name: ");
            String newName = scanner.nextLine();
            System.out.print("Please provide your surname");
            String newSurname = scanner.nextLine();

            if (!newName.equals("")) {
                newUser.setName(newName);
            }

            if (!newSurname.equals("")) {
                newUser.setSurname(newSurname);
            }

            if (userService.addUser(newUser)) {
                System.out.println("User " + newLogin + " successfully added to database");
            } else {
                System.out.println("Something went wrong. User not added to database!");
            }

        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("Going back to Guest Menu");
        }
    }

    public void bookCar() {
        try {

            Reservation newReservation = new Reservation();

            int pickVariable;
            do {
                System.out.println("Booking a car...");
                System.out.print("Please provide car brand: ");
                String carBrand = scanner.nextLine();
                printCars(carService.getCarsByBrand(carBrand));     //TODO Need some input verification here
                System.out.println("");
                System.out.print("Please provide car name: ");      //TODO Need some input verification here
                String carName = scanner.nextLine();
                System.out.println("");
                System.out.print("Please provide booking date (YYYY-MM-DD): ");      //TODO Need some input verification here!!!
                String carBookingDate = scanner.nextLine();


                Car bookedCar = carService.getCar(carBrand, carName);
                int mysteriousVariableValue = mysteriousVariableService.getVariable(1).getGetMysteriousVariableValue();

                Double reservationPrice = priceCalculator.calculatePrice(bookedCar.getBasePrice(), mysteriousVariableValue, bookedCar.getType());

                newReservation.setUserID(loginVerificator.getLoggedUserId());
                newReservation.setCarID(bookedCar.getCarId());
                newReservation.setDate(LocalDate.parse(carBookingDate));
                newReservation.setReservationCost(reservationPrice);

                System.out.println("Your reservation");
                printReservation(newReservation);

                System.out.println("Do you want to proceed with that reservation, or do you want to book a different car?");
                System.out.print("1. Proceed, 2. Book different car, 0. Go back to user menu");
                pickVariable = scanner.nextInt();
                scanner.nextLine();

                if (pickVariable == 0)
                    return;
            } while (pickVariable != 1);

            if (reservationService.addReservation(newReservation)) {
                System.out.println("Reservation added to the database");
            } else {
                System.out.println("Something went wrong. Reservation not added to the database");
            }

        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("Going back to User Menu");
        }
    }
}
