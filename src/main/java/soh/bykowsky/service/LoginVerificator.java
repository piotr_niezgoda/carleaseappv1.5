package soh.bykowsky.service;

public interface LoginVerificator {
    boolean verifyAdmin(String login, String password);
    boolean verifyUser(String login);
    boolean verifyUser(String login, String password);

    public int getLoggedUserId();
}
