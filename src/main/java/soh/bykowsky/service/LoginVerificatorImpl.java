package soh.bykowsky.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import soh.bykowsky.model.User;

@Component
public class LoginVerificatorImpl implements LoginVerificator {

    private int loggedUserId;       // hold info which user is currently logged in.

    @Autowired
    UserService userService;

    @Override
    public boolean verifyAdmin(String login, String password) {
        User user = userService.getUser(login);

        if (user.getLogin() != null && user.getUserType().equals("admin")) {
            if (user.getPassword().equals(password))
                loggedUserId = userService.getUser(login).getUserId();
                return true;
        }
        return false;
    }

    @Override
    public boolean verifyUser(String login) {
        User user = userService.getUser(login);

        if (user.getLogin() != null && !user.getUserType().equals("admin")) {
            loggedUserId = userService.getUser(login).getUserId();
            return true;
        }
        return false;
    }

    @Override
    public boolean verifyUser(String login, String password) {
        User user = userService.getUser(login);

        if (user.getLogin() != null && !user.getUserType().equals("admin")) {
            if (user.getPassword().equals(password))
                loggedUserId = userService.getUser(login).getUserId();
                return true;
        }
        return false;
    }

    public int getLoggedUserId() {
        return loggedUserId;
    }
}
