package soh.bykowsky.service;

import org.springframework.stereotype.Component;
import soh.bykowsky.DBhandler;
import soh.bykowsky.model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserServiceImpl implements UserService {

    public User getUser(int id) {
        try {
            String SQL = "SELECT * FROM Tusers WHERE  userID = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, id);
            ResultSet results = preparedStatement.executeQuery();

            User userFromDB = new User();
            while (results.next()) {
                userFromDB.setUserId(id);
                userFromDB.setLogin(results.getString("login"));
                userFromDB.setPassword(results.getString("password"));
                userFromDB.setUserType(results.getString("userType"));
                userFromDB.setName(results.getString("name"));
                userFromDB.setSurname(results.getString("surname"));
            }

            return userFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getUser(String login) {
        try {
            String SQL = "SELECT * FROM Tusers WHERE  login = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, login);
            ResultSet results = preparedStatement.executeQuery();

            User userFromDB = new User();
            while (results.next()) {
                userFromDB.setUserId(results.getInt("userID"));
                userFromDB.setLogin(login);
                userFromDB.setPassword(results.getString("password"));
                userFromDB.setUserType(results.getString("userType"));
                userFromDB.setName(results.getString("name"));
                userFromDB.setSurname(results.getString("surname"));
            }

            return userFromDB;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean addUser(User user) {
        try {
            String SQL = "INSERT INTO Tusers (login, password, userType, name, surname) VALUES (? , ?, ?, ?, ?)";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getUserType());
            preparedStatement.setString(4, user.getName());
            preparedStatement.setString(5, user.getSurname());

            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean deleteUser(int id) {
        try {
            String SQL = "DELETE FROM Tusers WHERE userID = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean deleteUser(String login) {
        try {
            String SQL = "DELETE FROM Tusers WHERE login = ?";
            PreparedStatement preparedStatement = DBhandler.connection.prepareStatement(SQL);
            preparedStatement.setString(1, login);
            preparedStatement.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
