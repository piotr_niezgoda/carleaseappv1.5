package soh.bykowsky.service;

import soh.bykowsky.model.User;

public interface UserService {
    User getUser(int id);
    User getUser(String login);
    boolean addUser(User user);
    boolean deleteUser(int id);
    boolean deleteUser(String login);
}
