package soh.bykowsky.service;

import soh.bykowsky.model.Reservation;

public interface ReservationService {
    Reservation getReservation(int reservationID);
    boolean addReservation(Reservation reservation);
    boolean deleteReservation(int reservationID);
}
