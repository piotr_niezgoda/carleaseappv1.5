package soh.bykowsky.service;

import soh.bykowsky.model.MysteriousVariable;

public interface MysteriousVariableService {
    MysteriousVariable getVariable(int variableID);
    boolean updateVariable(int variableID, int variableValue);
    boolean addVariable(MysteriousVariable mysteriousVariable);
    boolean deleteVariable(int variableID);
}
