package soh.bykowsky.model;

public class Car {
    private int carId;
    private String brand;
    private String name;
    private double basePrice;
    private String subType;
    private String type;

    public Car() {
    }

    public Car(String brand, String name, double basePrice, String subType) {
        this.brand = brand;
        this.name = name;
        this.basePrice = basePrice;
        this.subType = subType;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", brand='" + brand + '\'' +
                ", name='" + name + '\'' +
                ", basePrice=" + basePrice +
                ", subType='" + subType + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    // getters and setters
    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
