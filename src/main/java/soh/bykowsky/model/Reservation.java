package soh.bykowsky.model;

import java.time.LocalDate;

public class Reservation {
    private int reservationID;
    private int userID;
    private int carID;
    private LocalDate date;
    private double reservationCost;
    //TODO Think through the problem of different timezones and stuff like that

    public Reservation() {
    }

    public Reservation(int userID, int carID, LocalDate date, double reservationCost) {
        this.userID = userID;
        this.carID = carID;
        this.date = date;
        this.reservationCost = reservationCost;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "reservationID=" + reservationID +
                ", userID=" + userID +
                ", carID=" + carID +
                ", date=" + date +
                ", reservationCost=" + reservationCost +
                '}';
    }

    // getters and setters
    public int getReservationID() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getCarID() {
        return carID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getReservationCost() {
        return reservationCost;
    }

    public void setReservationCost(double reservationCost) {
        this.reservationCost = reservationCost;
    }
}
