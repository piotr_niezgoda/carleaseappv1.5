package soh.bykowsky;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        DBhandler.connect();


        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(2);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

        MainAppLoop mainAppLoop = context.getBean(MainAppLoop.class);
        MysteriousVariableGenerator mysteriousVariableGenerator = context.getBean(MysteriousVariableGenerator.class);

        threadPool.schedule(mainAppLoop,0, TimeUnit.SECONDS);
        threadPool.schedule(mysteriousVariableGenerator,0, TimeUnit.SECONDS);

        threadPool.shutdown();
    }
}
