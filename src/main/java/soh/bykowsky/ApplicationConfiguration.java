package soh.bykowsky;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import soh.bykowsky.service.*;

@Configuration
public class ApplicationConfiguration {

    @Bean
    DBhandler dBhandler(){
        return new DBhandler();
    }

    @Bean
    UserService userService(){
        return new UserServiceImpl();
    }

    @Bean
    CarService carService(){
        return new CarServiceImpl();
    }

    @Bean
    ReservationService reservationService(){
        return new ReservationServiceImpl();
    }

    @Bean
    MysteriousVariableService mysteriousVariableService(){
        return new MysteriousVariableServiceImpl();
    }

    @Bean
    MysteriousVariableGenerator mysteriousVariableGenerator(){
        return new MysteriousVariableGenerator();
    }

    @Bean
    UserInterface userInterface(){
        return new UserInterface();
    }

    @Bean
    MainAppLoop mainAppLoop(){
        return new MainAppLoop();
    }

    @Bean
    LoginVerificator loginVerificator(){
        return new LoginVerificatorImpl();
    }

    @Bean
    PriceCalculator priceCalculator(){
        return new PriceCalculatorImpl();
    }
}
