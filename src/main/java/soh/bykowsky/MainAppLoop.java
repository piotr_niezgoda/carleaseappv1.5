package soh.bykowsky;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import soh.bykowsky.service.CarService;
import soh.bykowsky.service.LoginVerificator;
import soh.bykowsky.service.UserInterface;
import soh.bykowsky.service.UserService;

import java.util.InputMismatchException;
import java.util.Scanner;

@Component
public class MainAppLoop implements Runnable {
    public static final String NO_SUCH_OPTION = "No such option available. Please try again.";

    private boolean running = true;
    private Scanner scanner = new Scanner(System.in);

    @Autowired
    UserInterface userInterface;

    @Autowired
    CarService carService;

    @Autowired
    LoginVerificator loginVerificator;

    @Autowired
    UserService userService;

    @Autowired
    MysteriousVariableGenerator mysteriousVariableGenerator;

    @Override
    public void run(){      // main menu logic and also main loop of the app
        try {
            boolean quit = false;
            while (!quit) {

                userInterface.printMainMenu();

                int userChoice = scanner.nextInt();
                scanner.nextLine();
                switch (userChoice) {
                    case 1:
                        System.out.println("Logging in as admin...");
                        System.out.print("Please provide login: ");
                        String adminLogin = scanner.nextLine();

                        System.out.print("Please provide password: ");
                        String adminPassword = scanner.nextLine();

                        if (loginVerificator.verifyAdmin(adminLogin, adminPassword)) {
                            adminMenuLogic();
                        } else {
                            System.out.println("Incorrect login, not an Admin or wrong password");
                        }
                        System.out.println("");
                        break;
                    case 2:
                        System.out.println("Logging in as user...");
                        System.out.print("Please provide user login: ");
                        String userLogin = scanner.nextLine();

                        System.out.print("Please provide password: ");
                        String userPassword = scanner.nextLine();

                        if (loginVerificator.verifyUser(userLogin, userPassword)) {
                            userMenuLogic();
                        } else {
                            System.out.println("Incorrect login or wrong password");
                        }
                        System.out.println("");
                        break;
                    case 3:
                        guestMenuLogic();
                        break;
                    case 4:
                        break;
                    case 0:
                        running = false;
                        quit = true;
                        mysteriousVariableGenerator.killThread();
                        break;
                }
            }
        } catch (InputMismatchException e) {
            e.printStackTrace();
//            System.out.println("Going back to main menu");
        }
    }

    private void adminMenuLogic() {
        try {
            boolean quit = false;
            while (!quit) {
                userInterface.printAdminMenu();
                //TODO printout "logged in as *login*

                int userChoice = scanner.nextInt();
                switch (userChoice) {
                    case 1: //TODO Change this shit like in case 6 and case 7
                        System.out.println("Here are all our cars/vehicles");
                        System.out.println("******************************");
                        userInterface.printCarsWithID(carService.getCarsAll());
                        break;
                    case 2: //TODO Change this shit like in case 6 and case 7
                        System.out.println("Here are our civilian cars");
                        System.out.println("******************************");
                        userInterface.printCarsWithID(carService.getCarsByType("osobowy"));
                        break;
                    case 3: //TODO Change this shit like in case 6 and case 7
                        System.out.println("Here are our trucks");
                        System.out.println("******************************");
                        userInterface.printCarsWithID(carService.getCarsByType("ciężarowy"));
                        break;
                    case 4: //TODO Change this shit like in case 6 and case 7
                        System.out.println("Here are our construction vehicles");
                        System.out.println("******************************");
                        userInterface.printCarsWithID(carService.getCarsByType("maszyna"));
                        break;
                    case 5:
                        carService.addCar(userInterface.addNewCar());

                        break;
                    case 6:
                        userInterface.duplicateCar();
                        break;
                    case 7:
                        userInterface.updateCar();
                        break;
                    case 8:
                        carService.deleteCar(userInterface.deleteCar());        //TODO Change this shit like in case 6 and case 7
                        break;
                    case 0:
                        quit = true;
                        break;
                    default:
                        System.out.println(NO_SUCH_OPTION);
                        break;
                }
            }
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("Going back to main menu");
        }
    }

    private void guestMenuLogic() {
        try {
            boolean quit = false;
            while (!quit) {
                userInterface.printGuestMenu();
                //TODO printout "logged in as *guest*

                int userChoice = scanner.nextInt();
                switch (userChoice) {
                    case 1:
                        System.out.println("Here are all our cars/vehicles");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsAll());
                        break;
                    case 2:
                        System.out.println("Here are our civilian cars");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsByType("osobowy"));
                        break;
                    case 3:
                        System.out.println("Here are our trucks");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsByType("ciężarowy"));
                        break;
                    case 4:
                        System.out.println("Here are our construction vehicles");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsByType("maszyna"));
                        break;
                    case 5:
                        userInterface.registerUser();
                        break;
                    case 0:
                        quit = true;
                        break;
                    default:
                        System.out.println(NO_SUCH_OPTION);
                        break;
                }
                System.out.println("");
            }
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("Going back to main menu");
        }
    }

    private void userMenuLogic(){
        try {
            boolean quit = false;
            while (!quit) {
                userInterface.printUserMenu();
                //TODO printout "logged in as *login*

                int userChoice = scanner.nextInt();
                switch (userChoice) {
                    case 1:
                        System.out.println("Here are all our cars/vehicles");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsAll());
                        break;
                    case 2:
                        System.out.println("Here are our civilian cars");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsByType("osobowy"));
                        break;
                    case 3:
                        System.out.println("Here are our trucks");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsByType("ciężarowy"));
                        break;
                    case 4:
                        System.out.println("Here are our construction vehicles");
                        System.out.println("******************************");
                        userInterface.printCars(carService.getCarsByType("maszyna"));
                        break;
                    case 5:
                        userInterface.bookCar();
                        break;
                    case 6:
                        //TODO
                        // Implement listing all of user bookings
                        break;
                    case 0:
                        quit = true;
                        break;
                    default:
                        System.out.println(NO_SUCH_OPTION);
                        break;
                }
                System.out.println("");
            }
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("Going back to main menu");
        }
    }

    public boolean isRunning() {
        return running;
    }
}
