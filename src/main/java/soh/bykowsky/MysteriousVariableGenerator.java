package soh.bykowsky;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import soh.bykowsky.service.MysteriousVariableService;

import java.util.Random;

@Component
public class MysteriousVariableGenerator implements Runnable {

    private Thread currentThread;

    @Autowired
    MysteriousVariableService mysteriousVariableService;

    @Autowired
    MainAppLoop mainAppLoop;

    @Override
    public void run() {
        currentThread = Thread.currentThread();

        boolean quit = false;
        do {
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
//                e.printStackTrace();
            }

            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(30)+1;
            mysteriousVariableService.updateVariable(1, randomInt);
        } while (mainAppLoop.isRunning());

    }

    public void killThread() {
        currentThread.interrupt();
    }
}
