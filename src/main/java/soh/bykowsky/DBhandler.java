package soh.bykowsky;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBhandler {
    public static Connection connection = null;

    public static void connect(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            DBhandler.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/CarLeaseApp?user=root&password=abecadlo");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
